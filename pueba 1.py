
def abrir(Arch):
    try:
        Archivo=open(Arch,'r')
    except:
        return 'El archivo no existe'
    Datos=Archivo.readlines()
    return leer(Datos,0,[])

def leer(Arch,ind,txt):
    if ind==len(Arch):
        return txt
    linea=arch(Arch[ind],'',[])
    return leer(Arch,ind+1,txt+[linea])

def arch(Linea,txt,L_lista):
    
    if len(Linea)==0:
        if txt=='':
            return L_lista
        if L_lista[-1]!= txt:
            return L_lista+[txt]
        return L_lista
    if Linea[0]=='\n':
        if txt=='':
            return arch(Linea[1:],txt,L_lista)
        return arch(Linea[1:],txt,L_lista+[txt])
    if Linea[0]=='\t':
        if txt=='':
            return arch(Linea[1:],txt,L_lista)
        return arch(Linea[1:],'',L_lista+[txt])
    if Linea[0]==' ':
        
        return arch(Linea[1:],txt,L_lista)
    return arch(Linea[1:],txt+Linea[0],L_lista)
        
def may_muj(Arch):
    Lista=abrir(Arch)
    Cantones=columna(Lista,0,0,[])
    Mujeres=columna(Lista,0,1,[])
    May_muj=mayor(Mujeres,1,0)
    return Cantones[May_muj],Mujeres[May_muj]
                    
def columna(List,Ind,Col,Datos):
    if Ind==len(List):
        return Datos
    return columna(List,Ind+1,Col,Datos+[List[Ind][Col]])
def mayor(L,Ind,Ind_may):
    if Ind==len(L):
        return Ind_may
    if L[Ind]>L[Ind_may]:
        return mayor(L,Ind,Ind)
    return mayor(L,Ind+1,Ind_may)

def men_hom(Arch):
    Lista=abrir(Arch)
    Cantones=columna(Lista,0,0,[])
    Hombres=columna(Lista,0,2,[])
    Men_hom=menor(Hombres,1,0)
    return Cantones[Men_hom],Hombres[Men_hom]
                    
def menor(L,Ind,Ind_men):
    if Ind==len(L):
        return Ind_men
    if L[Ind]<L[Ind_men]:
        return mayor(L,Ind,Ind)
    return menor(L,Ind+1,Ind_men)     
